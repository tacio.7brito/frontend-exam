import Vue from 'vue'
import Tasks from '@/components/Tasks'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios);

describe('Tasks.vue', () => {
  it('Testando retorno de dados da API e mostrando a descrição do primeiro objeto', () => {
    const Constructor = Vue.extend(Tasks);
    const vm = new Constructor().$mount();

    vm.getTasks();

    setTimeout(() => {
      expect(vm.tasks[0].description)
        .toBe('Lavar carro');
    }, 2000);
  });
});