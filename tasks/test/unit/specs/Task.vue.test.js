import Vue from 'vue'
import Task from '@/components/Task'

// helper function that mounts and returns the rendered text
function getRenderedTask (Component, propsData) {
  const Constructor = Vue.extend(Component)
  const vm = new Constructor({ propsData: propsData }).$mount()
  return vm.task
}

describe('Task.vue', () => {
  it('Renderizando com diferentes props', () => {
    expect(getRenderedTask(Task, {
      task: {"id": 1, "order": 1, "completed": true, "description": "Lavar carro"}
    })).toEqual({"id": 1, "order": 1, "completed": true, "description": "Lavar carro"});
  })
})


// import Vue from 'vue'
// import HelloWorld from '@/components/HelloWorld'

// describe('HelloWorld.vue', () => {
//   it('should render correct contents', () => {
//     const Constructor = Vue.extend(HelloWorld)
//     const vm = new Constructor().$mount()
//     expect(vm.$el.querySelector('.hello h1').textContent)
//       .toEqual('Welcome to Your Vue.js App')
//   })
// })
