# Executando o projeto

### Passo 1 - Baixar o projeto

Realize o "clone" e/ou "pull" da aplicação

### Passo 2 - Acessar pasta do projeto

Acesse, via terminal, o diretório do projeto chamado "/tasks"

### Passo 3 - Instalar dependências do projeto

Excute, via terminal, o comando "npm install" para que sejam baixadas todas as dependências do projeto

### Passo 4 - Simular API REST

Conforme orientado no "README.md", para simular a API REST, utilize o [json-server](https://github.com/typicode/json-server) passando o arquivo `database.json`, localizado em "/database.json" como entrada. Todas as informações de instalação e uso da ferramenta estão no repositório da mesma.

SUGESTÃO: depois de instalado globalmente o json-server, execute o arquivo database.json via terminal com o seguinte comando: "json-server -p 3000 database.json", sendo "3000" o número da porta, o que pode ser outra de sua preferência.

A sugestão acima se deu pois, ao executar o comando "json-server --watch database.json", conforme orientado pelo repositório do json-server, o mesmo sofria interrupção em sua execução de tempo em tempo.

### Passo 5 - Configurar URL da API

Abra o arquivo "main.js" localizado em "/tasks/src/main.js", na linha 15, informe a URL da API, conforme URL da API REST, caso necessário, como exemplo abaixo:

`axios.defaults.baseURL = 'http://localhost:3000/'`

### Passo 6 - Executar aplicação

Execute, via terminal, no diretório "/tasks", o comando "npm run dev", e aguarde enquanto a aplicação é inicializada. Após isso, abra no navegador a url apontada no terminal. Pronto, a aplicação já pode ser usada.

### Passo 7 - Executar testes unitários

Execute, via terminal, no diretório "/tasks", o comando "npm test", e aguarde enquanto os testes são executados. Os arquivos de teste estão localizados em "/tasks/test/unit/specs/".